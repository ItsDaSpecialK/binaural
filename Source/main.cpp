//
//  main.cpp
//  APCS Project
//
//  Created by Konrad Kraemer on 5/14/13.
//  Copyright (c) 2013 Konrad Kraemer. All rights reserved.
//

#include "main.h"

/// A struct for containing an hrtf.
struct hrtf {

	/// The stereo hrtf audio stream.
	Mix_Chunk* p3dStream;

	/// A buffer to store previously played sound in, since it is used in convolution.
	void* pbuf;

	/// Was intended for reversing the hrtf channels to allow the sound to come from the left side,
	/// but was never implemented.
	bool reverse;
};

int main(int argc, char* argv[]) {
	const char* songPath = "Binaural.app/Contents/Resources/Song.wav";

//	const char* hrtfPath = "Binaural.app/Contents/Resources/H0e000a.wav"; //front
	const char* hrtfPath = "Binaural.app/Contents/Resources/H0e090a.wav"; //right
//	const char* hrtfPath = "Binaural.app/Contents/Resources/H0e180a.wav"; //back


	SDL_Surface* screen;                 // Pointer to the main screen surface
	Mix_Chunk* sound = NULL;             // Pointer to our sound, in memory
	int channel;                         // Channel on which our sound is played

	int audio_rate = 44100;              // Frequency of audio playback
	Uint16 audio_format = AUDIO_S16SYS;  // Format of the audio we're playing
	int audio_channels = 2;              // 2 channels = stereo
	int audio_buffers = 1024;            // Size of the audio buffers in memory

	// Initialize BOTH SDL video and SDL audio
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		printf("Unable to initialize SDL: %s\n", SDL_GetError());
		return 1;
	}

	// Initialize SDL_mixer with our chosen audio settings
	if (Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) != 0) {
		printf("Unable to initialize audio: %s\n", Mix_GetError());
		return 1;
	}

	// Load our WAV file from disk
	sound = Mix_LoadWAV(songPath); // path to sound file
	if (sound == NULL) {
		printf("Unable to load WAV file: %s\n", Mix_GetError());
		return 1;
	}

	// Set the video mode to anything, just need a window
	screen = SDL_SetVideoMode(320, 240, 0, SDL_ANYFORMAT);
	if (screen == NULL) {
		printf("Unable to set video mode: %s\n", SDL_GetError());
		return 1;
	}

	// Play our sound file, and capture the channel on which it is played
	channel = Mix_PlayChannel(-1, sound, 0);
	if (channel == -1) {
		printf("Unable to play WAV file: %s\n", Mix_GetError());
		return 1;
	}

	hrtf* mine = new hrtf;

	// Try to load the hrtf
	mine->p3dStream = Mix_LoadWAV(hrtfPath); // path to hrtf
	if (mine->p3dStream == NULL) {
		printf("Unable to load WAV file: %s\n", Mix_GetError());
		delete mine;
		return 1;
	}
	mine->reverse = false;
	mine->pbuf = new Sint8[mine->p3dStream->alen]; // allocate the buffer.

	for (int i = 0; i < mine->p3dStream->alen; i++) // and fill it with 0's
		((Sint8*)mine->pbuf)[i] = 0;

	// Try to register the hrtf effect with SDLmixer.
	if (!Mix_RegisterEffect(channel, binaural, NULL, mine)) {
		printf("Mix_RegisterEffect: %s\n", Mix_GetError());
	}



	// Wait for keystroke to stop.
	std::cout << "Press a key to End";
	std::cin.ignore();

	// Release the memory allocated to our sound
	Mix_FreeChunk(sound);

	Mix_FreeChunk(mine->p3dStream);
	delete[] (Sint8*)mine->pbuf;
	delete mine;

	// Need to make sure that SDL_mixer and SDL have a chance to clean up
	Mix_CloseAudio();
	SDL_Quit();

	// Return success!
	return 0;
}

void convolve(Sint16* data, int len, Mix_Chunk* myhrtf, void* stream) {
	// Variable to store the sum in (lookup formula for convolution if confused).
	Sint64 sum;

	// Variable to store each summand.
	Sint64 temp;

	// loop variables.
	int k, j;

	// the number of indices in the hrtf.
	int hrtfIndexes = myhrtf->alen / 2; // alen is in bytes, hrtf is stored in 16 bit ints, so divide by 2.

	// magic coefficent which should prevent integer overflow.
	Sint32 d = 1024;

	// The number of indices in one channel of the hrtf.
	Sint32 hrtfChanLen = myhrtf->alen / 4; // 4, since 2 * 2-byte channels.

	// first ear (left I believe)
	for (int i = 0; i < len; i += 2) {
		sum = 0;
		for (j = i, k = hrtfIndexes - 2; j < len && k >= 0; j += 2, k -= 2) {
			temp = ((Sint16*)myhrtf->abuf)[k] * ((Sint16*)data)[j];
			temp /= d; // divide by d so there will not be an integer overflow error.
			sum += temp;
		}
		sum /= hrtfChanLen;

		// clamp if there is an overflow.
		if (sum > 32767) {
			std::cout << "NOOOOOOOOOOO"; // shouldn't happen
			sum = 32767;
		} else if (sum < -32768) {
			std::cout << "NOOOOOOOOOOO"; // shouldn't happen
			sum = -32768;
		}

		((Sint16*)stream)[i] = sum;
	}
	// second ear (right?)
	for (int i = 1; i < len; i += 2) {
		sum = 0;
		for (j = i, k = hrtfIndexes - 1; j < len && k >= 0; j += 2, k -= 2) {
			temp = ((Sint16*)myhrtf->abuf)[k] * ((Sint16*)data)[j];
			temp /= d; // divide by d so there will not be an integer overflow error.
			sum += temp;
		}
		sum /= hrtfChanLen;

		// clamp if there is an overflow.
		if (sum > 32767) {
			std::cout << "NOOOOOOOOOOO"; // shouldn't happen
			sum = 32767;
		} else if (sum < -32768) {
			std::cout << "NOOOOOOOOOOO"; // shouldn't happen
			sum = -32768;
		}

		((Sint16*)stream)[i] = sum;
	}
}

void binaural(int chan, void* stream, int len, void* udata) {
	// The hrtf this is convolving with.
	hrtf& myHrtf = *((hrtf*)udata);

	// The number of indices of Sint16's in stream.
	int numIndexes = len / 2; // len is the number of bytes. since it is an array of int16's, there are half as many indexes as there are bytes

	// The number of indices of Sint16's in hrtf.
	int hrtfStreamLength = myHrtf.p3dStream->alen / 2;

	// The length for the data field.
	int wholeLength = numIndexes + hrtfStreamLength;

	// make an array to store the stream, and the end of the last passed stream in.
	Sint16* data = new Sint16[wholeLength];

	// loop var for an index in data.
	int r = 0;

	// write remainder of the last stream into data.
	for (int i = 0; i < hrtfStreamLength; i++, r++)
		data[r] = ((Sint16*)myHrtf.pbuf)[i]; // write old pbuf to beginning of data

	// fill the rest of data, with the current stream.
	for (int i = 0; i < numIndexes; i++, r++)
		data[r] = ((Sint16*)stream)[i]; // write stream to data

	// take end of stream and save it into pbuf.
	for (int i = hrtfStreamLength - 1, j = numIndexes - 1; i >= 0; i--, j--)
		((Sint16*)myHrtf.pbuf)[i] = ((Sint16*)stream)[j]; // write end of stream to pbuf

	// actually performs the convolution.
	convolve(data, numIndexes, myHrtf.p3dStream, stream);

	delete[] data;
}
