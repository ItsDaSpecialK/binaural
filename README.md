# README #

This repository is based on an end-of-semester project I did in 10th grade (2013) for AP Computer Science.

### About 'Binaural' ###

The main purpose for this project was to learn more about sound spatialization, and how binaural audio works. Since I had less than a month to work on it, it has very little of the features I had originally planned for it. All it does, is play a sound file called `Song.wav` in such a way that it appears to come from a direction.

### How do I get set up? ###

- Note that this only works on macOS
- Download and install [SDL 1.2](https://www.libsdl.org/download-1.2.php)
- Download and install [SDLmixer 1.2](https://www.libsdl.org/projects/SDL_mixer/release-1.2.html)
- Download the [HRTF's](https://bitbucket.org/ItsDaSpecialK/binaural/downloads/hrtf.zip) from the downloads page, and extract into the resources folder
- Convert a **mono** song into a **stereo WAV** file, and place it in Resources as Song.wav (or change the path in main.cpp)

### Contribution guidelines ###

- Comment your code.
- Try to follow the current style.

### Questions? ###

* Contact Me!
* [kraemerk@gatech.edu](mailto:kraemerk@gatech.edu)
