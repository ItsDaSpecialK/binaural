//
//  main.h
//  APCS Project
//
//  Created by Konrad Kraemer on 5/14/13.
//  Copyright (c) 2013 Konrad Kraemer. All rights reserved.
//

#ifndef __APCS_Project__Main__
#define __APCS_Project__Main__

#include <iostream>
#include "SDL/SDL.h"
#include "SDL_mixer/SDL_mixer.h"

/** This method convolves a data stream against a 16-bit mix chunk.
 *  @param  data    The array which gets convolved with the hrtf. The length is the length of stream + the length of the hrtf.
 *  @param  len     The length of stream.
 *  @param  myhrtf  The hrtf to convolve with.
 *  @param  stream  The stream in which to output the result.
 */
void convolve(Sint16* data, int len, Mix_Chunk* myhrtf, void* stream);

/** This is a binaural effect function which gets automatically called by sdl.
 *  @param  chan    The channel of the audio stream (unused).
 *  @param  stream  The stream of audio data which serves as the input and output.
 *  @param  len     The number of bytes in stream.
 *  @param  udata   The field which is used to pass in an hrtf.
 */
void binaural(int chan, void* stream, int len, void* udata);

#endif /* defined(__APCS_Project__Main__) */
